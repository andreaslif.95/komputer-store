/**
 * Basic representation of the person wanting to buy the computer.
 */
class Person {
    constructor() {
        this.bankBalance = 0
        this.payCheck = 0
        this.loans = 0
    }

    canLoan() {
        return this.loans < 1
    }

    doLoan() {
        if(this.canLoan()) {
            this.bankBalance *= 2
            this.loans++
            return true
        }
        return false;
    }

    work() {
        this.payCheck += 100
    }

    getWorkBalance() {
        return this.payCheck
    }
    //Transfers money from the paycheck to the bank
    transfer() {
        this.bankBalance += this.payCheck
        this.payCheck = 0
    }

    getBalance() {
        return this.bankBalance
    }

    removeFromBalance(remove) {
        this.bankBalance -= remove
    }
    
}