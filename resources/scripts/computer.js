//Basic computer class.
class Computer {
    constructor(price, name, description, features, imgSrc) {
        this.price = price
        this.name = name
        this.description = description
        this.features = features
        this.imgSrc = imgSrc
    }   
}