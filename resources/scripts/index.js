let person
let currentChoice = 0
const computers = [
    new Computer(1500, "Lenovo 3250", "A amazing computer which usually works but sometimes doesn't", ["Working keyboard", "Mouse Pointer", "Amazing look"], "https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_73823293/fee_240_148_png/APPLE-MacBook-Air-%282020%29-13.3%22-B%C3%A4rbar-Dator---Silver"),
    new Computer(2500, "Areg 320v", "This is the new race car of computers, will you be able to handel it?", ["Overheating", "Bubbling plastic", "No fabric"], "https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_73823295/fee_240_148_png/APPLE-MacBook-Air-%282020%29-13.3%22-B%C3%A4rbar-Dator---Guld"),
    new Computer(3500, "Supero 23x", "Not good not bad but amazingly beautiful", ["Overheating sometimes", "Over priced", "Extremly fast"], "https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_73234769/fee_240_148_png/ASUS-VivoBook-14-R424DA-EK245T---14%22-B%C3%A4rbar-Dator"),
   new Computer(4500, "Crax lo", "Fortnite, or fornite? Which nite?", ["OK processor", "-32 IMBD", "Fast enough to play fortnite"], "https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_74572159/fee_240_148_png/APPLE-MacBook-Pro-%282020%29-13.3%22-B%C3%A4rbar-Dator---Gr%C3%A5-%28MXK32KS-A%29")

]

/**
 * OnLoad of the body we call this function to render every necessary part of the page and also to create a person object.
 */
function load() {
    person = new Person()

    let options = ""
    for(let i = 0; i < computers.length; i++) {
        options += `<option value="${i}">${computers[i].name}</option>`
    }
    document.getElementById("computerSelection").innerHTML = options
    renderComputer(computers[0])
    renderCash()
    loadAds()

}
/**
 * This will execute functions based on the type sent in.
 * Created to remove code duplication.
 * @param {} t - the type of the button action. 
 */
function buttonAction(t) {
    switch(t) {
        case 'loan':
            if(!person.doLoan()) {
                alert("You cant loan more money!")
            }    
            break;
        case 'work':
            person.work()
            break;
        case 'transfer':
            person.transfer()
            break;
        case 'buy':
            if(person.getBalance() >= computers[currentChoice].price) {
                person.removeFromBalance(computers[currentChoice].price)
                renderCash()
                confirm("You sucessfully purchased a computer! Page will reload...")
                document.location.reload()
            }
            else {
                alert('Sorry you dont have enough money.. Try to work more.')
            }
    }
    renderCash();
}
/**
 * When we select a computer we want to rener it.
 * @param {} tagSelection 
 */
function selectComputer(tagSelection) {
    currentChoice = tagSelection.value
    renderComputer(computers[tagSelection.value])
}
/**
 * Renders all money parts of the page.s
 */
function renderCash() {
    document.getElementById("workBalance").innerText = person.getWorkBalance() + "kr"
    document.getElementById("currentBalance").innerText = person.getBalance() + "kr"
}

/**
 * used to hide ads.
 */
function hide(t) {
    if(confirm("Are you sure you want to close this add?")) {
        t.parentElement.style.display = 'none'
    }
}

/**
 * Used to dynamically load the ads
 */
function loadAds() {
    let timer = 0
    let times = 1
    while(times < 4) {
        timer += (Math.random()*10000)
        console.log(timer)
        let id = "ad"+times
        setTimeout(() => {
            document.getElementById(id).style.display = "block"
        },timer)
        times++
    }
}
