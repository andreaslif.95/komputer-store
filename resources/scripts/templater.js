/**
 * Updates the inner HTML of computerHolder to match the chosen computer.
 * @param {} computer - Computer holding the information being rendered.
 */
function renderComputer(computer) {
    let featureString = `Features:`
    computer.features.forEach(features => {
        featureString += `<ul>${features}</ul>`
    })
    document.getElementById('computerHolder').innerHTML = 
                `<img class="computerImage" src="${computer.imgSrc}"></img>
                    <div class="innerItem">
                        <h2>${computer.name}</h2>
                        <p>${computer.description}</p>
                        <p>${featureString}</p>
                    </div>
                    <div class="innerItem">
                        <h2>${computer.price} kr</h2>
                        <button class="loanButton buyButton" onclick="buttonAction('buy')">Buy now</button>
                    </div>`;
}